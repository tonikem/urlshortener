<?php
include_once("includes/dbh.inc.php");
session_start();
error_reporting(E_ALL ^ E_NOTICE);
date_default_timezone_set('Europe/Helsinki');
$GLOBALS['chars'] = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
function generate(){
    $random = '';
    for ($i = 0; $i < 4; $i++){
        $random .= $GLOBALS['chars'][rand(0, 61)];
    }
    return "?".$random;
}
if (isset($_SESSION['urls']) === false) {$_SESSION['urls'] = array();}
if (isset($_SESSION['short']) === false) {$_SESSION['short'] = array();}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet">
    <title>Free url shortener</title>
    <link rel="stylesheet" href="style.css" type="text/css"/>
</head>
<body>
<div class="flex-container">
<?php
$server = $_SERVER['REQUEST_URI'];
if (preg_match("/[^\/]+$/", $server)) {
    echo "Something went wrong 🙄";
    if (strlen($server) === 6) {
        $short_url = substr($server, 1, 5);
        $s = "SELECT url FROM urls WHERE short_url='$short_url';";
        $result = mysqli_query($conn, $s);
        $row = mysqli_fetch_assoc($result);
        if (!empty($row['url'])) {
            $r = $row['url'];
            header("Location: $r");
        }
    }
}
elseif ($_SERVER['REQUEST_METHOD'] === 'POST' || 'GET') {
    include("templates/index.html");
    $url = mysqli_real_escape_string($conn, $_POST['url']);
    ini_set('error.silent', TRUE);
    $regex = "/^((?:http(?:s)?\:\/\/)?[a-zA-Z0-9_-]+(?:.[a-zA-Z0-9_-]+)*.[a-zA-Z]{2,4}(?:\/[a-zA-Z0-9_]+)*(?:\/[a-zA-Z0-9_]+.[a-zA-Z]{2,4}(?:\?[a-zA-Z0-9_]+\=[a-zA-Z0-9_]+)?)?(?:\&[a-zA-Z0-9_]+\=[a-zA-Z0-9_]+)*)$/";
    $check = @get_headers($url);
    if (preg_match($regex, $url)) {
        if (strpos($check[0], '200') === false) {
            echo "Url not found 😴";
        } else {
            if (in_array($url, $_SESSION['urls']) === false) {
                $s = "SELECT short_url FROM urls WHERE url='$url';";
                $result = mysqli_query($conn, $s);
                $row = mysqli_fetch_assoc($result);
                if (empty($row['short_url'])) {
                    $r = generate();
                    $sql = "INSERT INTO urls (url, short_url) VALUES ('$url', '$r');";
                    mysqli_query($conn, $sql);
                    array_push($_SESSION['short'], $r);
                } else {
                    array_push($_SESSION['short'], $row['short_url']);
                }
                array_push($_SESSION['urls'], $url);

                echo "Your url has been shortened 😁";
            } else {
                echo "This url is already shortened 😯";
            }
        }
    } else {
        if (empty($url)) {
            echo "Waiting for your url 😐";
        } else {
            echo "Can't shorten this url 😠";
        }
    }
    include("templates/list.php");
} else {header("HTTP/1.1 405 Method Not Allowed");}
?>
</div>
</body>
</html>
